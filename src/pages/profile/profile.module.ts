import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import {PipesModule} from "../../pipes/pipes.module";
import {LazyLoadImageModule} from "ng2-lazyload-image";

@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    PipesModule,
    LazyLoadImageModule
  ],
})
export class ProfilePageModule {}

