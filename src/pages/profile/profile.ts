import { Component, OnInit } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  ModalController, AlertController
} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import {AddWorkComponent} from "../../components/add-work/add-work";
import {EditProfileComponent} from "../../components/edit-profile/edit-profile";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {File} from "@ionic-native/file";
import {ImagePicker} from "@ionic-native/image-picker";
import {FileChooser} from "@ionic-native/file-chooser";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {HttpClient, HttpHeaders} from "@angular/common/http";



@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit {

  public user:any;
  private portfolio:boolean = false ;
  private contact:boolean  = true;
  // checks the visitor type
  private visitor:string;
  //checks is user profile is loaded or not
  private is_user_profile_loaded:boolean = false;
  private profile_loading:boolean = false;

  /* To ensure which kind of file is selected */
  private isCameraImage:boolean = false;
  private isGalleryImage:boolean = false;
  private post_data:any; // store the profile pic data that we are goingh to send to the server
  private host:string = "http://35.185.133.176:27017";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public authService:AuthProvider,
              public loadingCtrl:LoadingController,
              public toastCtrl:ToastController,
              public navParam:NavParams,
              private  modalCtrl:ModalController ,
              private alertCtrl:AlertController ,
              private file:File,
              private imagePicker:ImagePicker,private fileChooser:FileChooser,
              private fileTransfer:FileTransfer,
              private camera:Camera,
              private http:HttpClient,
              private auth:AuthProvider){
  }

  ngOnInit(){
    this.visitor = this.navParams.data['visitor'];

    console.log(this.visitor);
    if(this.visitor == 'user'){

      if(this.is_user_profile_loaded)
        return;

      this.loadUser();

    }else if(this.visitor == 'general'){

      //  fetch profile from server using profile id
      const loading  = this.loadingCtrl.create({ content:'Loading Profile..' , spinner:'dots' ,duration:4000});
      loading.present();
      this.authService.getProfileById(this.navParam.data['id'])
        .subscribe( (payload)=>{
          loading.dismiss(true);
          this.user = payload;
          this.user.name = this.navParam.data['creator_name'];
          console.log(this.navParam.data['creator_name']);
        },error1 => {
          loading.dismiss();
         // console.log(error1)
        });

      //check whether profile completely loaded or not
      loading.onDidDismiss( ref=>{
        if(!ref){
          this.showToast('Unable to load profile data')
        }
      })

    }else if(this.visitor == 'find_alig'){
      this.user = this.navParam.data['profile_data'];
    }

  }

  // load current user
  loadUser() {
    //getting already loader user profile from auth service
    // loader user after login

    this.auth.getProfile()
      .subscribe(ref=>{
        if(!ref)
          return;
        this.user = ref;
        //  console.log(this.user);
        //set the flag that profile is being loaded
       this.is_user_profile_loaded = true;

        this.auth.setUser(this.user);

      },err=>{

      })

  }

  ionViewDidLoad(){

  }

  showToast(msg){
    this.toastCtrl.create({
      message:msg,
      position:'top',
      duration:2000
    }).present();
  }

  editProfile(){
    const modal =   this.modalCtrl.create('EditProfileComponent' , this.user);
    modal.present();

    modal.onDidDismiss( ref =>{

      if(!ref)
        return;
      this.user = ref;
      console.log(ref)

    })

  }

  goBack(){
    this.navCtrl.pop()
  }

  onPortfolio(){
    this.portfolio = true;
    this.contact = false
  }
  onContact(){
    this.portfolio = false;
    this.contact = true;
  }

  addExperience(mode , data , i ){
    const modal =  this.modalCtrl.create('AddWorkComponent' , {mode:mode , data:data , index:i});
    modal.present();

    modal.onDidDismiss(ref =>{
      if(!ref)
        return;
      console.log(ref);
      if(ref['operation'] == 'add') {

        if (this.user.experience != undefined)
          this.user.experience.push(ref['data']);
        else {
          this.user.experience = [];
          this.user.experience.push(ref['data'])
        }

      }
      console.log(this.user)
    })

  }

  addEducation(mode  , data , i){

    const modal =  this.modalCtrl.create('AddEducationComponent' , {mode:mode , data:data , index:i});
    modal.present();

    modal.onDidDismiss(ref =>{
      if(!ref)
        return;
      console.log(ref);
      if(ref['operation'] == 'add') {

        if (this.user.education != undefined)
          this.user.education.push(ref['data']);
        else {
          this.user.education = [];
          this.user.education.push(ref['data']);
        }
      }
      console.log(this.user)
    })
  }

  deleteEducation(education , i){

    const id = education._id;
    const loading = this.loadingCtrl.create({
      content:'Removing item..'
    });
    loading.present();
    this.authService.deleteEducation(id)
      .subscribe(ref=>{
        if(!ref)
          return;
        this.user.education.splice(i , 1);
        loading.dismiss();
        this.showToast('Section deleted')
      },err=>{
        console.log(err);
        loading.dismiss();
        this.showToast('Network problem')
      })

  }

  deleteExperience(work , i){

    const id = work._id;
    const loading = this.loadingCtrl.create({
      content:'removing item..'
    });
    loading.present();
    this.authService.deleteEducation(id)
      .subscribe(ref=>{
        if(!ref)
          return;
        this.user.experience.splice(i , 1);
        this.showToast('Section deleted');
        loading.dismiss();
      },err=>{
        console.log(err);
        loading.dismiss();
        this.showToast('Network problem')
      })
  }

  deleteAlert(data , index  , item_type){

    this.alertCtrl.create({
      title:'Delete',
      message:'Are you sure ?',
      buttons:[
        {
          text:'Yes',
          handler: ()=>{
            if(item_type == 'education'){
              this.deleteEducation(data , index);
            }
            else{
              this.deleteExperience(data , index);
            }
          }
        },
        {
          text:'No',
          role:'cancel'
        }
      ]

    }).present();

  }

  settingsPage(){
    this.navCtrl.push('SettingsPage')
  }

  updateProfilePic(){
    const alert = this.alertCtrl.create({
      title:'Update profile picture',
      buttons:[
        {
          text:'Open Camera',
          handler: ()=>{
            this.takePicture();
          }
        },
        {
          text:'Open gallery',
          handler: ()=>{
            this.choseImage();
          }
        },
        {
          text:'Cancel',
          role:'cancel'
        }
      ]
    });
    alert.present();
  }

  takePicture(){
    const options: CameraOptions = {
      targetHeight: 700,
      targetWidth: 1000,
      allowEdit:true,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

      this.camera.getPicture(options).then((imageData) => {
      this.isCameraImage = true;


      this.post_data = imageData;

        this.uplaodProfile();

      console.log('cam_image_data ' +this.post_data);
    }, (err) => {
      // Handle error
      console.log(JSON.stringify(err))

    });

  }


  choseImage(){

    this.isGalleryImage = true;

    this.imagePicker.hasReadPermission().then( (result)=>{

      if(!result){
        this.imagePicker.requestReadPermission();
      }else{
        this.imagePicker.getPictures({
          maximumImagesCount:1
        }).then(images=>{

          console.log(images);
          for (var i = 0; i < images.length; i++) {
            this.post_data = images[i];
          }
          this.uplaodProfile();
          console.log(this.post_data);


        },err=>{
          console.log(JSON.stringify(err) + ' picker error')
        })
      }
    })
      .catch(err=>{
        console.log(JSON.stringify(err) + ' perission error')
      })

  }


  // @desc  To upload the selected file to the server

  uplaodProfile(){
    /* sending image using file transfer   */

    const loading = this.loadingCtrl.create({content: 'Uploading profile picture.. '});
    loading.present();
    const fileTransfer: FileTransferObject = this.fileTransfer.create();

    // creating proper file name with file extension
    if (this.post_data)
      var slash = this.post_data.lastIndexOf('/');
    let file_name: string;
    if (this.isCameraImage && this.post_data) {


      file_name = this.post_data.substring(slash + 1);
    }

    if (this.isGalleryImage && this.post_data) {
      //const question_mark = this.image_post.lastIndexOf('?');
      file_name = this.post_data.substring(slash + 1);
    }


    let options: FileUploadOptions = {
      fileKey: 'profile_picture',
      fileName: file_name,
      params: this.user,
      headers: {'Authorization': this.auth.getToken()}
    };
    // start file upload process
    fileTransfer.upload(this.post_data, 'http://35.185.133.176:27017/api/profile/', options)
      .then((data) => {
        if (!data) {
          return
        }

        // set the updated profile pic everywhere
        this.user = data;
        this.auth.setUser(data);

        //console.log(JSON.stringify(data));
        // this.viewCtrl.dismiss(data);
        loading.dismiss();

        this.showToast('Profile updated')
      }, (err) => {
        console.log(JSON.stringify(err));
        loading.dismiss();
        this.showToast('Upload failed');
      });

  }


  resetSelection(){

    this.post_data = "";

    if(this.isGalleryImage){
      this.isGalleryImage = false
    }

    if(this.isCameraImage){
      this.isCameraImage = false
    }


  }



}
