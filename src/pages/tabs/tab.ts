import { Component } from "@angular/core";
import { IonicPage, Platform } from "ionic-angular";


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tab.html'
})

export class TabPage{

  constructor(public platform:Platform ){
  }

  general_feed:any =  'FeedsPage';
  class_feed:any = 'ClassFeedPage';
  stories:any = 'StoriesPage'
  find_alig:any = "FindAligPage";
}



