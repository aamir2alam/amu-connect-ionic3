import { TabPage } from "./tab";
import { IonicPageModule } from "ionic-angular";
import { NgModule } from "@angular/core";


@NgModule({
  declarations: [TabPage,],
  imports: [IonicPageModule.forChild(TabPage),],
  exports: [TabPage,]
})

export class TabPageModule {
}

