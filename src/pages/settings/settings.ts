import { Component } from '@angular/core';
import {App, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public authService:AuthProvider , public app:App ,
              public loadingCtrl:LoadingController){
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  logout(){




    const loading = this.loadingCtrl.create({
      content:'Signing out ..',
      spinner:'dots',
    });
    loading.present();
    this.authService.deleteAuthStatus()
      .then(ref=>{

        console.log(JSON.stringify(ref));
        loading.dismiss();
        this.app.getRootNav().setRoot('LoginPage');
        this.navCtrl.setRoot('LoginPage')
      })
      .catch(err=>{
        loading.dismiss();

        console.log()
      });
  }

}
