import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindAligPage } from './find-alig';
import {PipesModule} from "../../pipes/pipes.module";
import {LazyLoadImageModule} from "ng2-lazyload-image";

@NgModule({
  declarations: [
    FindAligPage,
  ],
  imports: [
    IonicPageModule.forChild(FindAligPage),
    PipesModule,
    LazyLoadImageModule
  ],
})
export class FindAligPageModule {}
