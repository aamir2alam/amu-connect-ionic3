import { Component } from '@angular/core';
import {
  AlertController,
  IonicPage,
  Keyboard,
  LoadingController,
  ModalController,
  NavController,
  NavParams
} from 'ionic-angular';
import {AuthProvider} from "../../providers/auth/auth";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {constantCase} from "@ionic/app-scripts";


@IonicPage()
@Component({
  selector: 'page-find-alig',
  templateUrl: 'find-alig.html',
})
export class FindAligPage {

  private name:string;
  private faculty:string;
  private department:string;
  private year:string;
  private hall:string;

  private user:any;
  private aligs:any[]=[];

  private searching:boolean = false;
  private  searched:boolean = false;
  // creating local db url
  private  host:string = "http://35.185.133.176:27017";


  constructor(public navCtrl: NavController,private keyboard:Keyboard,
              public navParams: NavParams ,private auth:AuthProvider ,
              public http:HttpClient , public alertCtrl:AlertController) {
  }

  ionViewDidLoad() {

  }


  openUserProfile(){
    this.navCtrl.push('ProfilePage' , {visitor:'user'});
  }

  showAlert(title, msg){
    const alert = this.alertCtrl.create({
      title:title,
      message:msg,
      buttons:['OK']
    })
    alert.present()
  }

  findAlig(){

    this.searching = true;

      const url = this.host+ '/api/profile/find?department_name='+this.department +'&faculty_name='+this.faculty+'&passing_year='+this.year;
      const header = new HttpHeaders({
        'Authorization': this.auth.getToken()
      });

      try{
        this.http.get(url , {headers:header})
          .subscribe(payload=>{
            if(!payload)
              return;
            this.searching = false;
            this.searched = true;
            console.log(payload);
            let i=0;
            while(true){
              if(!payload[i]){
                break;
              }
              this.aligs.push(payload[i]);
              i++;
            }
            console.log(this.aligs);
            if(this.aligs.length == 0){
              this.showAlert('Try again','No record found please try again')
            }
          },err=>{
            this.searching = false;
          })
      }catch(err){
          this.searching = false;
      }

  }

  searchAgain(){
      this.searched = false;
      this.aligs = [];
  }

  openProile(alig){
    const id = alig.user._id;
    const creator_name = alig.user.name;
    this.navCtrl.push('ProfilePage' , {visitor:'find_alig' , id:id , profile_data:alig})

  }




  keyboardCheck() {
    if (this.keyboard.isOpen()) {
      let tabs = document.querySelectorAll('.show-tabbar');
      if (tabs !== null) {
        Object.keys(tabs).map((key) => {
          tabs[key].style.display = 'none';
        });
      }
    } else if (this.keyboard.didHide) {
      let tabs = document.querySelectorAll('.show-tabbar');
      if (tabs !== null) {
        Object.keys(tabs).map((key) => {
          tabs[key].style.display = 'flex';
        });
      }
    }
  }


  faculty_list = ["SHSSC",'Engineering' , 'Medicine' , 'Arts', 'Science' , 'Management' ,'Commerce' , 'Law', 'SocialScience',
    'LifeScience' , 'Unani', 'AgriculturalScience' , 'Theology'];

  depByCategory = {
    SHSSC: ["10+2","10"],
    Engineering: ["Applied Physics","Applied Chemistry","Applied Mathematics","Architecture","Chemical Engineering","Civil Engineering","Computer Engineering","Electrical Engineering","Electronics Engineering","Mechanical Engineering","Petroleum Studies","Others"],
    Medicine: ["Anatomy","Anesthesiology","Biochemistry","Cardiothoracic Surgery","Community Medicine","Dermatology","Ortho Laryngology (ENT)","Forensic Medicine","Medicine","Microbiology","Neuro Surgery","Obstetrics & Gynaecology","Opthalmology","Orthopaedic Surgery","Paediatrics","Paediatric Surgery","Pathology","Pharmacology","Physiology","Plastic Surgery","Psychiatary","Radio Diagnosis","Radio Therapy","Surgery","TB & Respiratory Diseases"],
    Arts: ["Arabic","English","Fine Arts","Hindi","Linguistics","Modern Indian Languages","Persian","Philosophy","Sanskrit","Urdu"],
    Science: ["Chemistry","Computer Science","Geography","Geology","Mathematics","Physics","Statistics and Operations Research","Remote Sensing and GIS Applications"],
    Management: ["Business Administration","New Management Complex"],
    Commerce: ["Commerce"],
    Law: ["Law"],
    SocialScience: ["Economics","Education","History","Islamic Studies","Library and Information Sciences","Mass Communication","Psychology","Physical Education","Political Science","Sociology","Social Work"],
    LifeScience: ["Biochemistry","Botany","Museology","Wildlife Sciences","Zoology"],
    Unani: ["All Unani Departments"],
    AgriculturalScience: ["All Agricultural Departments"],
    Theology:["Sunni","Shia"]
  }


}
