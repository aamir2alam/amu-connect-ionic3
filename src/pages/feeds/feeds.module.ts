import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedsPage } from './feeds';
import { PipesModule } from '../../pipes/pipes.module';
import {LazyLoadImageModule} from "ng2-lazyload-image";


@NgModule({
  declarations: [
    FeedsPage,
  
  ],
  imports: [
    IonicPageModule.forChild(FeedsPage),
    PipesModule,
    LazyLoadImageModule
  ],
})
export class FeedsPageModule {}



