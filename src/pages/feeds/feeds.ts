import {Component, OnInit} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ModalController,
  ToastController,
  AlertController,
  ViewController
} from 'ionic-angular';
import { FeedServicesProvider } from '../../providers/feed-services/feed-services';
import { AuthProvider } from '../../providers/auth/auth';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-feeds',
  templateUrl: 'feeds.html',
})
export class FeedsPage implements  OnInit{

  private posts:any[]=[];
  private loading:boolean = false;
  private is_loaded:boolean = false;
  private user:any;
  private updating_feed:boolean = false;
  private loading_profile:boolean = false;

  // to initially load first 10 moat recent feeds
  private chunk:number = 1;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public feedServices:FeedServicesProvider,
              public popoverCtrl:PopoverController,
              private modalCtrl:ModalController ,
              private auth:AuthProvider ,
              private alert:AlertController,
              private toastCtrl:ToastController, 
              ) {
  }

  showToast(msg){
    this.toastCtrl.create({message:msg , duration:2000, position:'top'})
  }

  ngOnInit(){
    this.loadUser();
    this.loadFeeds(1);
    console.log(this.posts);
  }


  ionViewDidEnter(){

    if(!this.is_loaded)
      return;

    this.updateFeed();

    let index = this.navParams.data['feed_index'];
    if(index == undefined)
      return;
    const is_liked = this.navParams.data['is_liked'];
   
    if(is_liked != this.posts[index].isLiked){
        if(is_liked){
          this.posts[index].isLiked = true;
          this.posts[index].likes_count++;
        }else{
          this.posts[index].isLiked = false;
          this.posts[index].likes_count--;
        }
    }
    let recieved_comments_counts = this.navParams.data['comments_count'];

    if(this.posts[index].comments_count != recieved_comments_counts){
      this.posts[index].comments_count = recieved_comments_counts;
    }


  }

  // load current user
  loadUser() {
    //getting already loader user profile from auth service
    // loader user after login
    this.loading_profile = true;
    this.auth.getProfile()
      .subscribe(ref=>{
        if(!ref)
          return;
        this.user = ref;
      //  console.log(this.user);
        this.loading_profile = false;
        this.auth.setUser(this.user);
          this.loading_profile = false;
      },err=>{

      })
  }

  openUserProfile(){
    this.navCtrl.push('ProfilePage' , {visitor:'user'});
  }

// To open other user profile
  openProfile(post){
    const id = post.profile_id._id;
    const creator_name = post.name;
    this.navCtrl.push('ProfilePage' , {visitor:'general' , id:id , creator_name:creator_name})
  }

  loadFeeds(chunkRef){
        this.loading = true;
    this.feedServices.getGeneralFeeds("class" , chunkRef)
                .subscribe(feedRef =>{
                  this.loading = false;
                  this.is_loaded = true;
                 if(!feedRef)
                      return;
              let i=0;
             for(; i < 10 ; i++) {
               if(!feedRef[i])
                 return;
               this.posts.push(feedRef[i]);
             }
            console.log(this.posts);

              this.loading = false;
             },err=>{
                  this.showToast('Unable to load feeds')
                  this.loading = true;
                });

  }

  // to check whether a node ia already present or not
  //filter out the nodes from loaded nodes that are not currently loaded
  Filter(payload) {

    function comparer(otherArray){
      return function(current){
        return otherArray.filter(function(other){
          return other._id == current._id
        }).length == 0;
      }
    }

    let  onlyInB ;

    if(this.posts.length > 10 ) {
       onlyInB = payload.filter(comparer(this.posts.slice(0, 10)));
    }else{
       onlyInB = payload.filter(comparer(this.posts));

    }

    return onlyInB;
  }


  
  doRefresh(refresher) {

    this.feedServices.refreshGeneralFeeds()
            .subscribe(ref=>{

      if(!ref)
        return;

      let filtered = this.Filter(ref).reverse();
              for( let ref of filtered){
                this.posts.unshift(ref);
              }

           refresher.complete();

    } , error1 => {
              refresher.complete();
              console.log(error1);
            });

  }

  // run when user post a feed

  updateFeed(){
    this.updating_feed = true;
    this.feedServices.refreshGeneralFeeds()
      .subscribe(ref=>{

        if(!ref)
          return;
         this.updating_feed = false;
        let filtered = this.Filter(ref).reverse();
        for( let ref of filtered){
          this.posts.unshift(ref);
        }

      } , error1 => {
        this.updating_feed = false;
      });

  }



 

      
   createFeed(){

      const modal = this.modalCtrl.create('CreateFeedComponent' , this.user);
      modal.present();

      modal.onDidDismiss(ref=>{
            
                  if(!ref)
                    return;

        this.updateFeed();

      })
   }


  like(post , i){

     if(!this.posts[i].isLiked){
       // update like status and count locally
        this.posts[i].isLiked  = true;
        this.posts[i].likes_count++;
       this.feedServices.likeFeed({feed:post._id })
         .subscribe( ref=>{
                if(!ref)
                  return;

         },err=>{
                  // reset changes if anything goes wrong
                    this.posts[i].isLiked = false;
                    this.posts[i].likes_count--;
                  
         })

     }else{
        // locally unlike the feed if it is already liked
        this.posts[i].isLiked  = false;
        this.posts[i].likes_count--; 
       this.feedServices.likeFeed({feed:post._id })
         .subscribe(ref=>{

                if(!ref)
                   return;

         },err=>{
           // reset changes if anything goes wrong
                this.posts[i].isLiked = true;
                this.posts[i].likes_count++;
         })
     }

}


  openFeedDetail(post , i , from ){
    this.navCtrl.push('FeedDetailPage' , {feed_data:post , opening_from:from, index:i ,type:'General'});
  }


  openMoreOptions(event , feed , index){

    const current_user_id  = this.user.user._id;
    const feed_id = feed._id;
    const feed_creator_id = feed.user;
    if(current_user_id == feed_creator_id){
      const options = [
        {
          text:'Delete',
          feed_id:feed_id,
          user_id:current_user_id,
          operation:'delete'
        }
      ];
      this.openPopover( event, options , index);
    }else{
      const options = [
        {
          text:'Report',
          feed_id:feed_id,
          user_id:current_user_id,
          operation:'report'
        }
      ];
      this.openPopover(event, options, index );
    }

  }
   
   
  openPopover(event , options , index){

    let popover= this.popoverCtrl.create('PopoverComponent' ,{options:options, from:'feed'} );
    popover.present({ev:event});
    
    popover.onDidDismiss(ref=>{
     // console.log(ref)
      if(!ref)
        return

        this.showToast('Feed Deleted')
        this.posts.splice(index , 1);
    })

  
    
  }


  doInfinite(): Promise<any> {

    return new Promise((resolve) => {
            this.chunk++;

          this.feedServices.getGeneralFeeds("class" ,this.chunk)
            .subscribe( payload=>{

              console.log(payload);

                  if(!payload)
                    return;
                  resolve(payload);

              for(let i=0; i < 10 ; i++) {
                if(!payload[i])
                  return;

                this.posts.push(payload[i]);
              }

            },err=>{
                
            })

    })
  }


}

