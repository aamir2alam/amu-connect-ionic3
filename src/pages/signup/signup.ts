import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  
  public name:string;
  public email:string;
  public institution_name:string = "Aligarh Muslim Univercity";
  public password:string;
  public passwordConfirm:string;


  constructor(public navCtrl: NavController, public navParams: NavParams,
              public toastCtrl:ToastController , private auth:AuthProvider,
              private loadingCtrl:LoadingController){
  }

  showToast(msg){
    this.toastCtrl.create({message:msg ,duration:2000,position:'top'}).present()
  }

  createAc(){

    if( this.name == undefined || this.email == undefined || this.password == undefined){
     this.showToast('Please enter all input fields');
      return
    }

    if( this.password != this.passwordConfirm){
     this.showToast('Both the passwords should be same');
      return
    }

    if(this.password.length < 6 ){
      this.showToast('Password must be atleast 6  characters');
      return
    }


   
    const payload  =  {
      name: this.name,
      email: this.email.toLowerCase(),
      institution_name: this.institution_name,
      password:this.password,
      password2:this.passwordConfirm
    };
      const loading = this.loadingCtrl.create({content:'Just a moment' , duration:4000, spinner:'dots'});
      loading.present();
      this.auth.signup(payload)
                  .subscribe(ref =>{
                    if(!ref )
                      return;
                    this.auth.setToken(ref['token']);
            loading.dismiss({status:0});
            this.showToast('Your account has been created');
            this.navCtrl.pop();
      }, err=>{
            loading.dismiss({status:err['status']});
          const err_string = err.error.email ? err.error.email : ""  +
                            err.error.password ? err.error.password : "" + 
                            err.error.institution_name ? err.error.institution_name : "" +
                            err.error.errors.email ? err.error.errors.email : "" ;  
        
        //const err_string = err.error.email + ' '+ err.error.password+ ' '+ err.errors.institution_name;
        this.toastCtrl.create({message:err_string  ,duration:2000,position:'top'}).present();

      });

    loading.onDidDismiss(ref=>{
      if(!ref){
        this.showToast('Server not responding please try again')
      }else{
        if(ref['status'] == 0){
          this.showToast('Network problem')
        }
        else{
          this.showToast('Server problem please try again')
        }
      }
    })

   }

}
