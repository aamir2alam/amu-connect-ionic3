import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';




@IonicPage()  
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',  
  
})
export class LoginPage {   

  private email:string;
  private password:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private authService:AuthProvider, private toastCtrl:ToastController,
              private loadingCtrl:LoadingController) {
  }

  ionViewDidLoad() {

  }
  showToast(msg){
      this.toastCtrl.create({message:msg , duration:2000, position:'top'}).present()
  }

    

  register(){
    this.navCtrl.push('SignupPage')
  }

  forgotPass(){
    this.toastCtrl.create({message:'Comming soon..',duration:2000,position:'top'}).present()
  }

  login(){

    if(this.email == undefined || this.password == undefined || this.email == "" || this.password == "") {
      this.toastCtrl.create({message:'Please fill email and password both',duration:2000,position:'top'}).present();
      return
    }
    if(this.password.length < 6){
      this.showToast('Password must be atleast 6  characters');
      return
    }

    const loading = this.loadingCtrl.create({content:'Just a moment' , spinner:'dots' , duration:4000});
    loading.present();
   this.authService.signin(this.email.toLowerCase() , this.password)
        .subscribe( ref=>{
            if(!ref)
              return;
          //@desc storing currently logged in user token
            this.authService.setToken(ref['token']);
          this.authService.storeAuthStatus(ref['token'])
          .then(ref=>{
              if(!ref)
                return;

          })
          .catch(err=>{

          });


          //this.showToast('Login Success');
          loading.dismiss({status:22});
          this.navCtrl.setRoot('TabPage')
          
        },err=>{

          loading.dismiss({status:err['status']});
        });

    loading.onDidDismiss(ref=>{
      if(!ref){
        this.showToast('Server not responding please try again')
      }else{
        if(ref['status'] == 0){
          this.showToast('Network problem')
        }
        else if(ref['status'] == 404){
          this.showToast('Account not found')
        }else if(ref['status'] == 400){
          this.showToast('Incorrect password')
        }

      }
    })
  }

}
