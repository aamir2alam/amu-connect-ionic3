import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {
  AlertController,
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  TextInput,
  ToastController
} from 'ionic-angular';
import {FeedServicesProvider} from "../../providers/feed-services/feed-services";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'page-feed-detail',
  templateUrl: 'feed-detail.html',
})
export class FeedDetailPage implements OnInit{

  @ViewChild('input') myInput :TextInput ;

  @Input() myInputData: any;
  @Output() sendToParent: EventEmitter<any> = new EventEmitter();

    private post:any;
    private feed_data:any;
    private feed_index:number;
    private feed_type:string;


    private comment_text:string;
    private is_Commenting:boolean = false;

    private page_type:string; // to check whther to open keyboard or detail page

    private is_comments_loading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams ,
              public toastCtrl:ToastController , private feedService:FeedServicesProvider,
              public alertCtrl:AlertController , public popoverCtrl:PopoverController,
              public auth:AuthProvider) {

  }


  ngOnInit(){

    this.feed_data = this.navParams.data['feed_data'];
    this.feed_index = this.navParams.data['index'];
    this.feed_type = this.navParams.data['type'];
    this.page_type = this.navParams.data['opening_from'];

  }

  public showToast(msg):void{
          const toast = this.toastCtrl.create({message:msg , duration:2000,});
          toast.present();
  }

  ionViewDidLoad() {

    if(this.page_type == 'comment'){
      setTimeout(() => {
        this.myInput.setFocus();
      },150);

    }

    // load all the comments form the server

    this.is_comments_loading = true;
    this.feedService.getFeedByID(this.feed_data._id)
      .subscribe( feedPayload=>{

        if(!feedPayload)
          return;

        this.post = feedPayload;
        this.is_comments_loading = false;
      },error=>{
        this.showToast('Unable to load comments');
        this.is_comments_loading = false;
      });


  }

  // To open other user profile
  openProfile(){
    const id = this.feed_data.profile_id._id;

    this.navCtrl.push('ProfilePage' , {visitor:'general' , id:id})
  }

  openCommentorProfile(comment_data){
    const profile_id = comment_data.profile_id;

  }


  like(){

    if(!this.feed_data.isLiked){
      // update like status and count locally
      this.feed_data.isLiked  =  true;
      this.feed_data.likes_count++;

      this.feedService.likeFeed({feed:this.feed_data._id })
        .subscribe( ref=>{
          if(!ref)
            return;


        },err=>{
        // rollback local changes if anything went wrong at the server side
          this.feed_data.isLiked = false;
          this.feed_data.likes_count--;
        })

    }else{

      this.feed_data.isLiked  = false;
      this.feed_data.likes_count--;

      this.feedService.likeFeed({feed:this.feed_data._id })
        .subscribe(ref=>{

          if(!ref)
            return;


        },err=>{
          // reset changes if anything goes wrong
          this.feed_data.isLiked = true;
          this.feed_data.likes_count++;
        })
    }

  }


  addComment(){

    if(this.comment_text == undefined || this.comment_text == ""){
      return
    }

    if(!this.post){
      return;
    }
    //create comment object
    const comment = {
      id:this.post._id,
      text:this.comment_text
    };
    this.is_Commenting = true;  // start processing the comment
    this.feedService.addComment(comment)
      .subscribe( ref=>{

            if(!ref)
              return;

        this.is_Commenting = false;
        this.post.comments = ref['comments'];
        this.comment_text = "";
        this.feed_data.comments_count++;

      },err=>{
        this.is_Commenting = false;
      })
  }

  ionViewWillLeave(){
    // this.navCtrl.getPrevious().data.like_count = this.likes_local;
     this.navCtrl.getPrevious().data.comments_count = this.feed_data.comments_count;
     this.navCtrl.getPrevious().data.feed_index = this.feed_index;
      this.navCtrl.getPrevious().data.is_liked = this.feed_data.isLiked;
  }

  // openMoreOptions(){
  //   const alert = this.alertCtrl.create({
  //     title:'choose you action',
  //     buttons:[
  //       {
  //         text:'Report',
  //         role:'cancel'
  //       },
  //       {
  //         text:'Delete',
  //         role:'cancel'
  //       },
  //       {
  //         text:'Share',
  //         role:'cancel'
  //       }
  //     ]
  //   });
  //
  //   alert.present();
  // }

  openMoreOptions(event , comment , index){
    console.log(comment);

    const current_user_id = this.auth.getCurrentUserId();
    const commentor_id = comment.profile_id;
    const comment_id = comment._id;
    const post_id = this.post._id;
    console.log(current_user_id );
    if(current_user_id == commentor_id){
      const options = [
        {
          text:'Delete',
          comment_id:comment_id,
          user_id:current_user_id,
          post_id:post_id,
          operation:'delete'
        }
      ];
      this.openPopover( event, options , index);
    }else{
      const options = [
        {
          text:'Report',
          comment_id:comment_id,
          user_id:current_user_id,
          post_id:post_id,
          operation:'report'
        }
      ];
      this.openPopover(event, options, index );
    }

  }


  openPopover(event , options , index){

    let popover= this.popoverCtrl.create('PopoverComponent' ,{options:options,from:'comment'} );
    popover.present({ev:event});

    popover.onDidDismiss(ref=>{
      // console.log(ref)
      if(!ref)
        return;

      this.showToast('Comment deleted');
      this.post.comments.splice(index , 1);
      this.feed_data.comments_count--;
    })



  }



}

