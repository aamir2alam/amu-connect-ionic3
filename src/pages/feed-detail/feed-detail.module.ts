import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedDetailPage } from './feed-detail';
import {PipesModule} from "../../pipes/pipes.module";
import {LazyLoadImageModule} from "ng2-lazyload-image";

@NgModule({
  declarations: [
    FeedDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedDetailPage),
    PipesModule,
    LazyLoadImageModule
  ],
})
export class FeedDetailPageModule {}
