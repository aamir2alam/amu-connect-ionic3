import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {
  AlertController,
  IonicPage,
  ModalController,
  NavController,
  NavParams,
  PopoverController,
  ToastController, ViewController
} from 'ionic-angular';
import {FeedServicesProvider} from "../../providers/feed-services/feed-services";
import {AuthProvider} from "../../providers/auth/auth";
import  {File} from '@ionic-native/file';
import {FileTransfer, FileTransferObject} from "@ionic-native/file-transfer";


@IonicPage()
@Component({
  selector: 'page-class-feed',
  templateUrl: 'class-feed.html',
})
export class ClassFeedPage implements OnInit{

  private posts:any[]=[];
  private loading:boolean = false;
  private is_loaded:boolean = false;
  private user:any;
  private updating_feed:boolean = false;

  // to initially load first 10 moat recent feeds
  private chunk:number = 1;


  private download_progress:number = 0 ;
  private is_downloading:boolean = false;
  private downloading_process:boolean[]=[];



  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public feedServices:FeedServicesProvider,
              public popoverCtrl:PopoverController,
              private modalCtrl:ModalController ,
              private auth:AuthProvider ,
              private alert:AlertController,
              private file:File,
              private fileTransferCtrl:FileTransfer,
              private toastCtrl:ToastController ,
              private viewCtrl:ViewController,
              private cd:ChangeDetectorRef
              ){}


  ionViewDidLoad(){

  }

  ionViewDidEnter(){


    // return if feeds are loading for the first time
    if(!this.is_loaded)
      return;

    this.updateFeed();

    let index = this.navParams.data['feed_index'];
    if(index == undefined)
      return;
    const is_liked = this.navParams.data['is_liked'];

    if(is_liked != this.posts[index].isLiked){
      if(is_liked){
        this.posts[index].isLiked = true;
        this.posts[index].likes_count++;
      }else{
        this.posts[index].isLiked = false;
        this.posts[index].likes_count--;
      }
    }
    let recieved_comments_counts = this.navParams.data['comments_count'];

    if(this.posts[index].comments_count != recieved_comments_counts){
      this.posts[index].comments_count = recieved_comments_counts;
    }

  }

  showAlert(msg:string){
    this.alert.create({
      message:msg,
      buttons:['OK']
    }).present();
  }

  showToast(msg){
    this.toastCtrl.create({message:msg , duration:2000, position:'top'})
  }

  ngOnInit(){

    this.loadUser();
    this.loadFeeds(1)

  }

  // load current user
  loadUser() {
    //getting already loaded user from service
    this.user = this.auth.getUser();
  }

  openUserProfile(){
    this.navCtrl.push('ProfilePage' , { visitor:'user'});
  }


  loadFeeds(chunkRef){
    this.loading = true;
    console.log('getting feeds');
    this.feedServices.getClassFeeds(chunkRef)
      .subscribe(feedRef =>{

        if(!feedRef)
          return;

        this.loading = false;
        this.is_loaded = true;


        for(let i=0; i < 10 ; i++) {

          if(!feedRef[i])
            return;

          this.posts.push(feedRef[i]);


          this.downloading_process.push(false);
        }

        this.loading = false;
        this.is_loaded = true;

        // this.loaded = true
        //  console.log(this.posts)
      },err=>{
        this.showToast('Unable to load feeds')
        this.loading = true;


      });


  }

  // Filter nodes form loaded nodes that are not present in the view
  Filter(payload) {

    function comparer(otherArray){
      return function(current){
        return otherArray.filter(function(other){
          return other._id == current._id
        }).length == 0;
      }
    }

    let  onlyInB ;

    if(this.posts.length > 10 ) {
      onlyInB = payload.filter(comparer(this.posts.slice(0, 10)));
    }else{
      onlyInB = payload.filter(comparer(this.posts));
    }
    return onlyInB;
  }



  doRefresh(refresher) {
    this.feedServices.refreshClassFeed()
      .subscribe(ref=>{

        if(!ref)
          return;

        let filtered = this.Filter(ref).reverse();
        for( let ref of filtered){
          this.posts.unshift(ref);


          this.downloading_process.unshift(false);
        }

        refresher.complete();

      } , error1 => {
        refresher.complete();
      });

  }

  // run when user post a feed

  updateFeed(){
    this.updating_feed = true;
    this.feedServices.refreshClassFeed()
      .subscribe(ref=>{

        if(!ref)
          return;
        this.updating_feed = false;
        let filtered = this.Filter(ref).reverse();
        for( let ref of filtered){
          this.posts.unshift(ref);

          this.downloading_process.unshift(false);
        }

      } , error1 => {

        this.updating_feed = false;
      });

  }





  openPopover(event){
    let popover= this.popoverCtrl.create('PopoverComponent' ,this.user );
    popover.present({ev:event});
  }

  createFeed(){
    const modal = this.modalCtrl.create('CreateFeedComponent' , this.user);
    modal.present();
    modal.onDidDismiss(ref=>{
      if(!ref)
        return
      this.updateFeed();
    })
  }

  like(post , i){

    if(!this.posts[i].isLiked){
      // update like status and count locally
      this.posts[i].isLiked  = true;
      this.posts[i].likes_count++;
      this.feedServices.likeFeed({feed:post._id })
        .subscribe( ref=>{
          if(!ref)
            return;

        },err=>{
          // reset changes if anything goes wrong
          this.posts[i].isLiked = false;
          this.posts[i].likes_count--;

        })

    }else{
      // locally unlike the feed if it is already liked
      this.posts[i].isLiked  = false;
      this.posts[i].likes_count--;
      this.feedServices.likeFeed({feed:post._id })
        .subscribe(ref=>{

          if(!ref)
            return;

        },err=>{
          // reset changes if anything goes wrong
          this.posts[i].isLiked = true;
          this.posts[i].likes_count++;
        })
    }

  }


  openProfile(post){
    const uid = post.user;
    this.navCtrl.push('ProfilePage' , {visitor:'general' , uid:uid})

  }

  openFeedDetail(post , i , from ){
    this.navCtrl.push('FeedDetailPage' , {feed_data:post , opening_from:from, index:i ,type:'Class'});
  }


  doInfinite(): Promise<any> {

    return new Promise((resolve) => {
      this.chunk++;

      this.feedServices.getClassFeeds(this.chunk)
        .subscribe( payload=>{


          if(!payload)
            return;
          resolve(payload);

          for(let i=0; i < 10 ; i++) {
            if(!payload[i])
              return;

            this.posts.push(payload[i]);

            this.downloading_process.push(false);
          }

        },err=>{
        })

    })
  }


   fileTransfer: FileTransferObject = this.fileTransferCtrl.create();
  download(data , i){
    this.download_progress = 0;

    if(this.is_downloading){
      this.showAlert('Please wait another download is in process');
      return;
    }

    let file_url =  this.auth.getUrl()+'/' + data.image_post;  //'http://172.20.10.2:27017/lol.pdf';  //data.image_post;   //"http://pepa.holla.cz/wp-content/uploads/2016/12/Learning-React-Native.pdf";
    let file_name = data.image_post;

        // exttract name and url

    // start downloading process
    this.is_downloading = true;

    this.downloading_process[i] = true;
    this.fileTransfer.download(file_url, this.file.externalRootDirectory+ file_name  ).then((entry) => {
       if(!entry)
         return;
       this.is_downloading = false;
       this.downloading_process[i] = false;
       this.showToast('File downloaded')

    }, (error) => {
      // handle error

      this.is_downloading = false;
      this.downloading_process[i] = false;
      this.showAlert('Download failed');

    });

    this.fileTransfer.onProgress((progressEvent) => {
      //data.isLiked = this.likes_array[i];
      if (progressEvent.lengthComputable) {
        var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);

        this.download_progress = perc;
       this.cd.detectChanges();


      } else {

      }
    });

  }

  downloadControl(i){
    if(this.is_downloading){

        this.alert.create({title:'Downloading..' ,buttons:[
            {
              text:'Cancel download',
              handler: ()=>{
                this.fileTransfer.abort();
                this.showToast('Download cancelled');
                this.is_downloading = false;
                this.downloading_process[i] = false;
                this.download_progress = 0 ;
              }
            },{
              text:'Continue',
              role:'cancel'
            }
          ]}).present();
    }
  }

  getFileType(feed_data){
  //    let file_name = feed_data.
  }

}
