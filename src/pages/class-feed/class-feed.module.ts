import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClassFeedPage } from './class-feed';
import {ProgressBarComponent} from "../../components/progress-bar/progress-bar";
import {LazyLoadImageModule} from "ng2-lazyload-image";

@NgModule({
  declarations: [
    ClassFeedPage,
    ProgressBarComponent
  ],
  imports: [
    IonicPageModule.forChild(ClassFeedPage),
    LazyLoadImageModule
  ],
})
export class ClassFeedPageModule {}
