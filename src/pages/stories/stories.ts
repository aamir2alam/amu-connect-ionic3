import { Component, OnInit } from '@angular/core';
import {IonicPage, NavController, NavParams, PopoverController} from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthProvider} from "../../providers/auth/auth";


@IonicPage()
@Component({
  selector: 'page-stories',
  templateUrl: 'stories.html',
})
export class StoriesPage implements OnInit{

  public feeds:any[]=[];
  private user:any;
  private url:string = "https://amunotes.com/news/amu-convocation-president-amunotes-com/";

  constructor(public navCtrl: NavController, public navParams: NavParams ,
            public theInAppBrowser:InAppBrowser , private auth:AuthProvider,
              public http:HttpClient, private popoverCtrl:PopoverController){
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StoriesPage');
  //  this.user = this.auth.getUser();
  }

  openUserProfile(){
    this.navCtrl.push('ProfilePage' , {profile_data : this.user , visitor:'user'});
  }

  openPopover(event){
    let popover= this.popoverCtrl.create('PopoverComponent' ,this.user );
    popover.present({ev:event});
    console.log('popovre')
  }

  openStory(url){
    window.open(url, '_system', 'location=yes');
    let header = new HttpHeaders({
      'Access-Control-Allow-Origin':'*'
    });

    this.http.get('https://amunotes.com/news/social-sciences-amunotes-com/',{headers:header})
      .subscribe(ref=>{
        if(!ref)
          return;
        console.log(ref);
      },error=>{
        console.log(error)
      })
  }

  ngOnInit(){

  }


  getWebCode(){
  }

  
options : InAppBrowserOptions = {
  location : 'yes',//Or 'no' 
  hidden : 'no', //Or  'yes'
  clearcache : 'yes',
  clearsessioncache : 'yes',
  zoom : 'yes',//Android only ,shows browser zoom controls 
  hardwareback : 'yes',
  mediaPlaybackRequiresUserAction : 'no',
  shouldPauseOnSuspend : 'no', //Android only 
  closebuttoncaption : 'Close', //iOS only
  disallowoverscroll : 'no', //iOS only 
  toolbar : 'yes', //iOS only 
  enableViewportScale : 'no', //iOS only 
  allowInlineMediaPlayback : 'no',//iOS only 
  presentationstyle : 'pagesheet',//iOS only 
  fullscreen : 'yes',//Windows only    
};
public openWithCordovaBrowser(url : string){
  let target = "_self";
  this.theInAppBrowser.create(url,target,this.options);
}
public openWithInAppBrowser(url : string){
  let target = "_blank";
  this.theInAppBrowser.create(url,target,this.options);
}
// openStory(url:string) {
//
//   this.openWithCordovaBrowser(url);
//
// }
public openWithSystemBrowser(url : string){
  let target = "_system";
  this.theInAppBrowser.create(url,target,this.options);
}

}
