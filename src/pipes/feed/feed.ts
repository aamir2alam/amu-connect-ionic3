import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'feed',
})


export class FeedPipe implements PipeTransform {

  transform(value:any) {
    console.log('feed pipe');

        const date = new Date(value);
        const time = date.getTime();
        console.log(time);

    return time;

  }
}
