import { NgModule } from '@angular/core';
import { FetchImagePipe } from './fetch-image/fetch-image';
import { FeedPipe } from './feed/feed';

@NgModule({
	declarations: [FetchImagePipe,
    FeedPipe],
	imports: [],
	exports: [FetchImagePipe,
    FeedPipe]
})
export class PipesModule {}
