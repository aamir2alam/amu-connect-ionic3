import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Injectable()
export class AuthProvider {

  // creating local db url
  private  host:string = "http://35.185.133.176:27017";  // "http://35.236.135.159:27017";
  private token:string;
  private user:any;

  constructor(private http:HttpClient , private storage: Storage) {
    console.log('Hello AuthProvider Provider');
  }

  getUrl(){
    return this.host;
  }

  signup(userRef) {
    const url  = this.host +"/api/users/register";
    const options = userRef;

    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type':  'application/x-www-form-urlencoded'
    //   })
    // };

    try{

      return this.http.post(url , options )

    }catch(err){
      //console.log('from http');
     // console.log(err)
    }


  }

  

  signin(email: string, password: string) {
    const url = this.host +"/api/users/login";
    const options ={email:email , password:password};
   try{
    return this.http.post(url , options)
   }catch(err){
    // console.log(err)
   }               
  }

  // key to store the auth status is 'jwt'

  storeAuthStatus(jwt){
   return  this.storage.set('jwt' ,jwt )
  }

  getAuthStatus(){
  return this.storage.get('jwt')
  }

  deleteAuthStatus(){
    return  this.storage.remove('jwt')
  }
  
  setToken(token) {
    console.log('token set  '+token);
     this.token = token;


  }

  setUser(user_data){
    this.user = user_data;
  }
  getUser(){
    return this.user;
  }
  getCurrentUserId(){
    return this.user._id;
  }

  getToken(){
    console.log('token set '+ this.token);
    return this.token
  }

  getProfile(){
      const url = this.host+"/api/profile/";
      const header = new HttpHeaders({
        'Authorization':this.token
      });
    //  const options = {Authorization:this.token}
     try{
      return  this.http.get(url , {headers:header} )
     }catch(err){
      //  console.log('from http');
       //  console.log(err)
     }
    
  }

  getProfileById(uid){
   // console.log('getting for ' + uid);
    const url  = this.host + "/api/profile/" +  uid;
    const header = new HttpHeaders({
      'Authorization':this.token
    });
    try{
      return this.http.get(url ,{headers:header})

    }catch(err){
     // console.log("http call error" +err)
    }
  }

  updateProfile(data){

    const url = this.host+"/api/profile/";
    const header = new HttpHeaders({
      'Authorization':this.token
    })
    //  const options = {Authorization:this.token}
    try{
      return  this.http.post(url , data, {headers:header} )
    }catch(err){
    //  console.log(JSON.stringify(err) + ' from service')
    }

  }

  addWork(data){

    const url = this.host+"/api/profile/experience";
    const header = new HttpHeaders({
      'Authorization':this.token
    })
    //  const options = {Authorization:this.token}
    try{
      return  this.http.post(url , data, {headers:header} )
    }catch(err){
    //  console.log(JSON.stringify(err) + ' from service')
    }

  }
  deleteWork(id){
    const url = this.host+"/api/profile/experience/" +id ;
    const header = new HttpHeaders({
      'Authorization':this.token
    })
    //  const options = {Authorization:this.token}
    try{
      return  this.http.delete(url , {headers:header} )
    }catch(err){
   //   console.log(JSON.stringify(err) + ' from service')
    }
  }

  updateWork(id){

  }

  addEducation(education_data){

    // school: req.body.school,
    //   degree: req.body.degree,
    //   fieldofstudy: req.body.fieldofstudy,
    //   from: req.body.from,
    //   to: req.body.to,
    //   current: req.body.current,
    //   description: req.body.description
    const url = this.host+"/api/profile/education";
    const header = new HttpHeaders({
      'Authorization':this.token
    })
    //  const options = {Authorization:this.token}
    try{
      return  this.http.post(url ,education_data, {headers:header} )
    }catch(err){
     // console.log(JSON.stringify(err) + ' from service')
    }
  }

  deleteEducation(id){

    const url = this.host+"/api/profile/education/" +id ;
    const header = new HttpHeaders({
      'Authorization':this.token
    })
    //  const options = {Authorization:this.token}
    try{
      return  this.http.delete(url , {headers:header} )
    }catch(err){
   //   console.log(JSON.stringify(err) + ' from service')
    }

  }
   
}
