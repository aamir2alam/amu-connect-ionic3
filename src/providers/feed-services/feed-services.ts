
import { Injectable } from '@angular/core';
import { AuthProvider } from '../auth/auth';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class FeedServicesProvider {

  private host:string =  "http://35.185.133.176:27017"   //"http://35.236.135.159:27017";

  constructor(private auth:AuthProvider ,
              private http:HttpClient) { 
  }


  // general feed operations
  public getGeneralFeeds(type:string , chunk){

      const url = this.host+ "/api/posts/institution/general/" +chunk;
      const header = new HttpHeaders({
        'Authorization': this.auth.getToken()
      });

      try{
        return  this.http.get(url , {headers:header})
      }catch(err){
        
      }

  }

  public  create_feed(body){
      const url  = this.host + "/api/posts";


      const header = new HttpHeaders({
        'Authorization' : this.auth.getToken()
        // 'Content-Type': 'application/x-www-form-urlencoded'

      });
    try {

        return this.http.post(url , body, {headers:header} )

    }catch (e) {
      
    }
  }



  public likeFeed(body){
    const url = this.host + "/api/posts/like/" + body.feed;

    console.log(body);

    const header = new HttpHeaders({
      'Authorization' : this.auth.getToken(),

    });
    try{

      return this.http.post(url  ,"",  {headers:header} )

    }catch (e) {
      
    }


  }

  // public unlikeFeed(body){
  //
  //   const url = this.host + "/api/posts/unlike/" + body.feed;
  //
  //   console.log(body);
  //
  //   const header = new HttpHeaders({
  //     'Authorization' : this.auth.getToken(),
  //
  //   });
  //   try {
  //
  //     return this.http.post(url , "", {headers:header} )
  //
  //   }catch (e) {
  //
  //   }
  //
  // }


  public getFeedByID(id){

    const url = this.host + "/api/posts/"+ id ;

    console.log('id ' +id);

    const header = new HttpHeaders({
      'Authorization' : this.auth.getToken(),

    });
    try {

      return this.http.get(url , {headers:header} )

    }catch (e) {

    }

  }

  public addComment(body){

    const url = this.host + "/api/posts/comment/"+ body.id;

    const header = new HttpHeaders({
      'Authorization' : this.auth.getToken(),

    });
    try {

      return this.http.post(url ,{text:body.text}, {headers:header} )

    }catch (e) {

    }

  }

    refreshGeneralFeeds(){
       // refresh feed url

      const url = this.host+ "/api/posts/institution/general/1";
      const header = new HttpHeaders({
        'Authorization': this.auth.getToken()
      });

      try{

         return this.http.get(url , {headers:header});

      }catch(err){
        
      }

    }

    // class feed operations

  public getClassFeeds(chunk){

    const url = this.host+ "/api/posts/class/"+ chunk;
    const header = new HttpHeaders({
      'Authorization': this.auth.getToken()
    });

    try{
      return  this.http.get(url , {headers:header})
    }catch(err){
   
    }

  }

  public refreshClassFeed(){
    const url = this.host+ "/api/posts/class/1";
    const header = new HttpHeaders({
      'Authorization': this.auth.getToken()
    });

    try{

      return this.http.get(url , {headers:header});

    }catch(err){
      
    }

  }

  deleteFeed(feed_id){
    const url = this.host + "/api/posts/"+feed_id;
    const header = new HttpHeaders({
      'Authorization': this.auth.getToken()
    });

    try{

      return this.http.delete(url , {headers:header});

    }catch(err){
     
    }
  }
  //api/posts/comment/:post_id/:comment_id
  deleteComment(commment_id , post_id){
    const url = this.host + "/api/posts/comment/"+post_id +"/"+commment_id;

    const header = new HttpHeaders({
      'Authorization': this.auth.getToken()
    });

    try{

      return this.http.delete(url , {headers:header});

    }catch(err){

    }

  }

}
