import { Component } from '@angular/core';
import {LoadingController, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AuthProvider} from "../providers/auth/auth";


 
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = 'LoginPage';
   
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
              loadingCtrl:LoadingController , authService:AuthProvider) {


    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
       const loading =  loadingCtrl.create({content:'Checking auth status' , spinner:'dots'});
        authService.getAuthStatus()
          .then(ref=>{
            if(!ref)
              return;
            console.log(ref);
            loading.dismiss();
            authService.setToken(ref);
            this.rootPage = 'TabPage';
          })
          .catch(err=>{
            console.log(err);
            loading.dismiss();
            this.rootPage = 'LoginPage'
          });


      statusBar.styleDefault();
      if (platform.is('android')) {
          statusBar.overlaysWebView(false);
          statusBar.backgroundColorByHexString('primary');
      }
      
      splashScreen.hide();
    });
  }    
}

