import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { AuthProvider } from '../providers/auth/auth';
import { FeedServicesProvider } from '../providers/feed-services/feed-services';
import { StoryServicesProvider } from '../providers/story-services/story-services';
import { HttpClientModule } from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Camera} from '@ionic-native/camera';

// adding image lazyloading module
import { LazyLoadImageModule } from 'ng2-lazyload-image';
import {FileChooser} from "@ionic-native/file-chooser";

import { FileTransfer} from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import {File}  from '@ionic-native/file'
import {ImagePicker} from "@ionic-native/image-picker";
import {IonicStorageModule} from "@ionic/storage";
import { NativePageTransitions } from '@ionic-native/native-page-transitions';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    LazyLoadImageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp], 
  entryComponents: [ 
    MyApp,
  ],   
  providers: [
    StatusBar,
    SplashScreen,
    HttpClientModule,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    FileChooser,
    InAppBrowser,
    FeedServicesProvider,
    StoryServicesProvider ,
    FileTransfer,
    FilePath,
    File,
    ImagePicker,
    NativePageTransitions

    
  ]
})
export class AppModule {}   
     