import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PipesModule} from "../../pipes/pipes.module";
import {EditProfileComponent} from "./edit-profile";

@NgModule({
  declarations: [
    EditProfileComponent
  ],
  imports: [
    IonicPageModule.forChild(EditProfileComponent),
    PipesModule
  ],
})
export class EditProfileModule {}
