import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavParams, ViewController} from "ionic-angular";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfileComponent implements  OnInit{

  private  user:any;

  constructor(private navParam:NavParams ,
              private loadingCtrl:LoadingController,
              private  viewCtrl:ViewController ,
              private  authService:AuthProvider ,
              private alertCtrl:AlertController) {}

ngOnInit(){
   this.user = this.navParam.data;

}

showAlert(msg ){

    this.alertCtrl.create(
      {
        title:'Failed!',
        subTitle:msg,
        buttons:['OK']
      }).present()

  }

  done(){

     const loading  = this.loadingCtrl.create({
       content:'Updating profile..',
       duration:4000
     });
     loading.present();

     this.authService.updateProfile(this.user)
           .subscribe(ref=>{
             if(!ref)
               return;
             loading.dismiss(true);
             this.viewCtrl.dismiss(this.user);

           },err=>{

             loading.dismiss(false);
             this.showAlert('Server not responding');
           })

    loading.onDidDismiss(ref=>{
      if(!ref){
        this.showAlert('Network problem please try again');
      }

    })

    }

  cancel(){
    // no updation
    this.viewCtrl.dismiss(false)

  }

  // local data for selection

  faculty_list = ["SHSSC",'Engineering' , 'Medicine' , 'Arts', 'Science' , 'Management' ,'Commerce' , 'Law', 'SocialScience',
    'LifeScience' , 'Unani', 'AgriculturalScience' , 'Theology']

  depByCategory = {
    SHSSC: ["10+2","10"],
    Engineering: ["Applied Physics","Applied Chemistry","Applied Mathematics","Architecture","Chemical Engineering","Civil Engineering","Computer Engineering","Electrical Engineering","Electronics Engineering","Mechanical Engineering","Petroleum Studies","Others"],
    Medicine: ["Anatomy","Anesthesiology","Biochemistry","Cardiothoracic Surgery","Community Medicine","Dermatology","Ortho Laryngology (ENT)","Forensic Medicine","Medicine","Microbiology","Neuro Surgery","Obstetrics & Gynaecology","Opthalmology","Orthopaedic Surgery","Paediatrics","Paediatric Surgery","Pathology","Pharmacology","Physiology","Plastic Surgery","Psychiatary","Radio Diagnosis","Radio Therapy","Surgery","TB & Respiratory Diseases"],
    Arts: ["Arabic","English","Fine Arts","Hindi","Linguistics","Modern Indian Languages","Persian","Philosophy","Sanskrit","Urdu"],
    Science: ["Chemistry","Computer Science","Geography","Geology","Mathematics","Physics","Statistics and Operations Research","Remote Sensing and GIS Applications"],
    Management: ["Business Administration","New Management Complex"],
    Commerce: ["Commerce"],
    Law: ["Law"],
    SocialScience: ["Economics","Education","History","Islamic Studies","Library and Information Sciences","Mass Communication","Psychology","Physical Education","Political Science","Sociology","Social Work"],
    LifeScience: ["Biochemistry","Botany","Museology","Wildlife Sciences","Zoology"],
    Unani: ["All Unani Departments"],
    AgriculturalScience: ["All Agricultural Departments"],
    Theology:["Sunni","Shia"]
  }



}
