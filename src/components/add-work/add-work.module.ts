import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PipesModule} from "../../pipes/pipes.module";
import {AddWorkComponent} from "./add-work";


@NgModule({
  declarations: [
    AddWorkComponent
  ],
  imports: [
    IonicPageModule.forChild(AddWorkComponent),
    PipesModule
  ],
})
export class AddWorkModule {}
