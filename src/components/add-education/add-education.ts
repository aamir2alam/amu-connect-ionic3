import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavParams, ViewController} from "ionic-angular";
import {AuthProvider} from "../../providers/auth/auth";

@IonicPage()
@Component({
  selector: 'add-education',
  templateUrl: 'add-education.html'
})
export class AddEducationComponent implements OnInit{

  private education:any ={};
  private index:number;
  private mode:string;
  constructor(private  view:ViewController , private navParams:NavParams,
              public authService:AuthProvider , public alertCtrl:AlertController,
              public loadingCtrl:LoadingController){
  }

  ngOnInit(){
    // check to either edit work or new work
    this.index = this.navParams.data['index'];
    this.mode = this.navParams.data['mode'];
    if(this.navParams.data['mode'] == 'edit'){
      this.education = this.navParams.data['data']
    }
    console.log(this.navParams.data['mode'])
  }

  close(){
    // send nothing if user cancelled

    this.view.dismiss(false)
  }

  showAlert(msg){
    this.alertCtrl.create({
      title:'Failed',
      message:msg,
      buttons:['OK']
    }).present();
  }

  addEducation(){
    // send word object to on dissmissed

    console.log(this.education);
    const loading = this.loadingCtrl.create({
      content:'Adding Education..',

    });
    loading.present();

    this.authService.addEducation(this.education)
      .subscribe(ref=>{
        if(!ref)
          return;
        console.log(ref);
        loading.dismiss();
        this.view.dismiss({data:this.education , operation:'add' , index:this.index})
      },err=>{
        console.log(err);
        loading.dismiss();
        this.showAlert('Something went wrong please try again')
      });
  }

  deleteEducation(){

    const id = this.education._id;
    this.authService.deleteEducation(id)
      .subscribe(ref=>{
        if(!ref)
          return;
        this.view.dismiss({data:ref , operation:'delete' , index:this.index})
      },err=>{
        console.log(err);
        this.showAlert('Something went  wrong please try again')
      })

  }



}
