import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PipesModule} from "../../pipes/pipes.module";
import {AddEducationComponent} from "./add-education";



@NgModule({
  declarations: [
    AddEducationComponent
  ],
  imports: [
    IonicPageModule.forChild(AddEducationComponent),
    PipesModule
  ],
})
export class AddEducationModule {}
