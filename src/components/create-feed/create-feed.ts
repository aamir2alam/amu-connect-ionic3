import {Component, ViewChild, ElementRef, OnInit, ChangeDetectorRef} from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ViewController, ToastController, NavParams, LoadingController, IonicPage} from 'ionic-angular';

import { CameraOptions, Camera } from '@ionic-native/camera';

import { AuthProvider } from '../../providers/auth/auth';
import {FileChooser} from "@ionic-native/file-chooser";

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import {File} from "@ionic-native/file";
import {ImagePicker} from "@ionic-native/image-picker";


@IonicPage()
@Component({
  selector: 'create-feed',
  templateUrl: 'create-feed.html'
})

/** important  plugin-file is required by file-chooser-plugin  */
export class CreateFeedComponent implements OnInit {


  @ViewChild('myInput') myInput: ElementRef;   // to set focus on keyboard


  private text:string;
  private image:string;
  private user:any;
  private post_data:string;   // to store data to be posted image,video / docs
  private fileData:any;    // to store file data to be posted

  /* To ensure which kind of file is selected */
  private isCameraImage:boolean = false;
  private isGalleryImage:boolean = false;
  private isFile:boolean = false;
  private previewLoading:boolean = false;
  private host:string = "http://35.185.133.176:27017";

  private upload_progress:number = 0 ;

  constructor(private http:HttpClient,
              private viewCtrl:ViewController,
              private fileChooser:FileChooser,
              private fileTransfer:FileTransfer,
              private toastCtrl:ToastController,
              private camera:Camera ,
              private  loadingCtrl:LoadingController,
              private navParam:NavParams,
              private auth:AuthProvider,
              private file:File,
              private imagePicker:ImagePicker,
              private cd :ChangeDetectorRef
             ) {}

              ngOnInit(){
              this.user = this.navParam.data
              }

      close(){
        this.viewCtrl.dismiss()
      }

      
      showToast(msg){
          this.toastCtrl.create({message:msg , duration:2000, position:'top'}).present()
      }

      // method to push creating feed
      createFeed(){
        /* sending image using file transfer   */

        if(this.isGalleryImage || this.isCameraImage  && this.post_data) {

          const loading = this.loadingCtrl.create({content: 'Uploading feed.. '});
          loading.present();
          const fileTransfer: FileTransferObject = this.fileTransfer.create();

          // creating proper file name with file extension
          if (this.post_data)
            var slash = this.post_data.lastIndexOf('/');
          let file_name: string;
          if (this.isCameraImage && this.post_data) {


            file_name = this.post_data.substring(slash + 1);
          }

          if (this.isGalleryImage && this.post_data) {
            //const question_mark = this.image_post.lastIndexOf('?');
            file_name = this.post_data.substring(slash + 1);
          }

          if (this.isFile && this.fileData) {
            file_name = 'docname'
          }

          let options: FileUploadOptions = {
            fileKey: 'post_data',
            fileName: file_name,
            params: {text: this.text , type:'general'},
            headers: {'Authorization': this.auth.getToken()}
          };
            // start file upload process"http://35.185.133.176:27017"
          fileTransfer.upload(this.post_data, 'http://35.185.133.176:27017/api/posts/', options)
            .then((data) => {
              if (!data) {
                return
              }
              this.viewCtrl.dismiss(data);
              loading.dismiss();

              this.showToast('File uploaded')
            }, (err) => {
              loading.dismiss();
              this.showToast('Upload failed');
            });

          fileTransfer.onProgress((progressEvent) => {
            if (progressEvent.lengthComputable) {
              var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
              this.upload_progress = perc;
              this.cd.detectChanges();

            } else {

            }
          });

        }else{


          const loading = this.loadingCtrl.create({content:'Creating feed.. '});
          loading.present();

          let formData:FormData = new FormData();
          formData.append('text', this.text);
          formData.append('type' , 'general');
          let headers1 = new Headers();

          /** In Angular 5, including the header Content-Type can invalidate your request */
          const url = this.host + "/api/posts";


          const header = new HttpHeaders({
            'Authorization' : this.auth.getToken()
          });

          this.http.post(url, formData, {headers: header}, )
            .subscribe( ref=>{
              this.showToast('suceess')
              loading.dismiss()
              this.viewCtrl.dismiss(true)
            } ,error1 => {
              loading.dismiss()
              this.viewCtrl.dismiss(false)
              this.showToast(error1.message)
            })

        }
      }



    takePicture(){
      this.resetSelection();
      const options: CameraOptions = {
      targetHeight: 700,
      targetWidth: 1000,
      allowEdit:true,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
      };

      this.camera.getPicture(options).then((imageData) => {
        this.isCameraImage = true;

        let filename = imageData.substring(imageData.lastIndexOf('/')+1);
        let path =  imageData.substring(0,imageData.lastIndexOf('/')+1);
        //then use the method reasDataURL  btw. var_picture is ur image variable

        this.file.readAsDataURL(path, filename).then(res=> {

          this.image = res;


        })
          .catch(err=>{
          });

        this.post_data = imageData;


      }, (err) => {
       // Handle error

      });

    }


    choseImage(){


      this.resetSelection();
     


        this.imagePicker.hasReadPermission().then( (result)=>{

            if(!result){
              this.imagePicker.requestReadPermission();
            }else{
              this.imagePicker.getPictures({
                maximumImagesCount:1
              }).then(images=>{

                  for (var i = 0; i < images.length; i++) {
                      this.post_data = images[i];
                  }

                  this.isGalleryImage = true;

                let filename = this.post_data.substring(this.post_data.lastIndexOf('/')+1);
                let path =  this.post_data.substring(0,this.post_data.lastIndexOf('/')+1);
                //then use the method reasDataURL  btw. var_picture is ur image variable

                 this.previewLoading = true;
                this.file.readAsDataURL(path, filename).then(res=> {
                          if(!res)
                            return;
                  this.image = res;
                  this.previewLoading = false;
                })
                  .catch(err=>{
                  });


              },err=>{
              })
            }
        })
          .catch(err=>{
          })

    }

    openGallery(){

      this.resetSelection();

      const options: CameraOptions = {
        targetWidth:700,
        targetHeight:1000,
        destinationType: this.camera.DestinationType.NATIVE_URI,
       // encodingType: this.camera.EncodingType.JPEG,
         mediaType: this.camera.MediaType.ALLMEDIA,
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        saveToPhotoAlbum:false
      };

      this.camera.getPicture(options).then((imageData) => {
        this.isGalleryImage = true;
        this.post_data = 'file://'+ imageData;

        let filename = this.post_data.substring(this.post_data.lastIndexOf('/')+1);
        let path =  this.post_data.substring(0,this.post_data.lastIndexOf('/')+1);
        //then use the method reasDataURL  btw. var_picture is ur image variable

        this.file.readAsDataURL(path, filename).then(res=> {

              this.image = res;

        })
          .catch(err=>{

          });

      }, (err) => {
       // Handle error

      });

    }

  getFileType(img_url){

    var dot = this.post_data.lastIndexOf('.');
    const file_type = this.post_data.substring(dot+1);
    return file_type;

  }

  resetSelection(){

    this.post_data = "";
    this.image = "";

    if(this.isGalleryImage){
      this.isGalleryImage = false
    }

    if(this.isCameraImage){
      this.isCameraImage = false
    }
    if(this.isFile) {
      this.isFile = false;
    }

  }
}
