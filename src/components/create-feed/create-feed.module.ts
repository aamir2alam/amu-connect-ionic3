import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PipesModule} from "../../pipes/pipes.module";
import {CreateFeedComponent} from "./create-feed";

@NgModule({
  declarations: [
    CreateFeedComponent
  ],
  imports: [
    IonicPageModule.forChild(CreateFeedComponent),
    PipesModule
  ],
})
export class CreateFeedModule {}
