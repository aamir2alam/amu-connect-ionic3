import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams, IonicPage, LoadingController, AlertController} from 'ionic-angular';
import { FeedServicesProvider } from '../../providers/feed-services/feed-services';


@IonicPage()
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent implements OnInit{


  public options:any[]=[];
  public popover_from:string;
  constructor(private navParam:NavParams,
              private viewCtrl:ViewController,
             private feedService:FeedServicesProvider ,
              private loadingCtrl:LoadingController,
              private alertCtrl:AlertController)  {
  }
  ngOnInit(){
    this.popover_from = this.navParam.data['from'];
    this.options = this.navParam.data['options'];
    console.log(this.popover_from)
  }

  close() {
    this.viewCtrl.dismiss();
}

report(option){
  this.close();
}

delete(option){
  const alert = this.alertCtrl.create({
    title:'Delete',
    message:'Are you sure',
    buttons:[
      {
        text:'Yes',
        handler: ()=>{
              if(this.popover_from == 'feed'){
                this.deleteFeed(option.feed_id);
              }else if(this.popover_from == 'comment'){
                this.deleteComment(option.comment_id , option.post_id)
              }

        }
      },
      {
        text:'No',
        role:'cancel'
      }
    ]
  })
  alert.present();

}

deleteFeed(feed_id){

  const loading = this.loadingCtrl.create({
    content:'Deleting',
    duration:5000
  });
  loading.present();
  this.feedService.deleteFeed(feed_id)
        .subscribe( ref=>{
          if(!ref)
            return;
            
            this.viewCtrl.dismiss(true);
            loading.dismiss()
        },err=>{
            loading.dismiss();
            this.viewCtrl.dismiss()
        })

}

deleteComment(comment_id , post_id){
  const loading = this.loadingCtrl.create({
    content:'Deleting',
    duration:5000
  });
  loading.present();
  this.feedService.deleteComment(comment_id , post_id)
    .subscribe( ref=>{
      if(!ref)
        return;

      this.viewCtrl.dismiss(true);
      loading.dismiss()
    },err=>{
      loading.dismiss();
      this.viewCtrl.dismiss()
    })
}



}
