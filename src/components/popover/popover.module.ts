import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PipesModule} from "../../pipes/pipes.module";
import {PopoverComponent} from "./popover";

@NgModule({
  declarations: [
    PopoverComponent
  ],
  imports: [
    IonicPageModule.forChild(PopoverComponent),
    PipesModule
  ],
})
export class EditProfileModule {}
